/**
 * 将虚拟 Dom 转化为真实 Dom
 */
function createRealDom(virtualNode, parentNode = null) {
  // 创建真实的DOM节点
  const domNode = document.createElement(virtualNode.tag);

  // 设置属性
  if (virtualNode.attrs) {
    for (const attr in virtualNode.attrs) {
      domNode.setAttribute(attr, virtualNode.attrs[attr]);
    }
  }

  // 添加子节点
  if (virtualNode.children && virtualNode.children.length > 0) {
    virtualNode.children.forEach(child => {
      const childNode = createRealDom(child);
      domNode.appendChild(childNode);
    });
  }

  // 如果有父节点，则添加到父节点中，否则直接返回根节点
  if (parentNode !== null) {
    parentNode.appendChild(domNode);
  }

  return domNode;
}
// 给定的虚拟DOM对象
const virtualDom = {
  tag: 'DIV',
  attrs: { id: 'app' },
  children: [
    {
      tag: 'SPAN',
      children: [{ tag: 'A', children: [] }],
    },
    {
      tag: 'SPAN',
      children: [
        { tag: 'A', children: [] },
        { tag: 'A', children: [] },
      ],
    },
  ],
};
// console.log(createRealDom(virtualDom))