/**
 * Object.assign(obj,obj1,obj2...)
 * 浅复制
 */
Object.myAssign = (target, ...source) =>{
  let res = Object(target);
  source.forEach((each) => {
    if(each!==null){
      for(let key of each){
        if(each.hasOwnProperty(key)){
          res[key] = each[key]
        }
      }
    }
  })
  return res;
}