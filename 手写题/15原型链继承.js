function People() {
  this.name = ['origin people'];
  this.sayHi = function(){
    console.log('hi')
  }
}
function Women() {}
Women.prototype = new People();
Women.prototype.constructor = Women

const rose = new Women();
rose.name.push('rose')
const lili = new Women();
lili.name.push('lili')
console.log(lili.name) // ['origin people', 'rose']
lili.sayHi()


/**
 * 原型链相关知识
 * 1 【得到对象】prototype (函数（原型）独有) 函数的原型对象 代表实例所共享的原型 
 *             prototype={constructor,[[Prototype]]} 
 * 2 【得到对象】__proto__ 或者[[Prototype]] (实例对象有) 实例的原型对象，构成原型链 
 *             ins.__proto__ = fn.prototype
 * 3 【得到函数】constructor (实例对象有) 构造函数
 * https://blog.csdn.net/cc18868876837/article/details/81211729
 */

/**
 * superType 是构造函数
 * 1 跟prototype的关系
 * 2 跟__proto__的关系
 * 任何函数在创建的时候，其实会默认同时创建该函数的prototype对象 有constructor属性
 * fn.prototype.constructor = fn
 * constructor属性的终点就是Function这个函数。
 */

/**
 * 如何访问原型对象？
 * 构造函数调用prototype属性
 * 实例调用__proto__属性，或者 Object.getPrototypeOf(实例)
 */
/**
 * superType.prototype = {constructor: f:superType, [[Prototype]]} 是superType实例的公共对象
 * (原型)superType.prototype = (实例) instance.__proto__
 */

/**
 * 实例 是一个对象 
 * {...属性, [[Prototype]]：{constructor,[[Prototype]]} }
 * ins.__proto__ = fn.prototype
 */

/**
 * new 构造函数得到实例 instance
 * 1.创建一个新对象 {}
 * 2.调用构造函数superType 内部的this指向新对象
 * 3.返回新对象的引用
 */