/**
 * XMLHTTPRequest对象
 * ajax是对它的封装
 * axios返回promise
 * fetch返回的是promise
 * JavaScript在发送AJAX请求时，URL的域名必须和当前页面完全一致（同源）
 */
const ajax = (method, url, data, isAsync=true) => {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
      if (xhr.readyState != 4) return;
      if (xhr.status == 200 || xhr == 304) { // 状态码是number， 304资源未被修改，客户端取缓存
        resolve(xhr.responseText);
      } else {
        reject(new Error(xhr.responseText));
      }
    };
    // xhr.setRequestHeader('Accept', 'application/json'); // 客户端期望接收的响应类型
    xhr.open(method, url, isAsync);
    xhr.send(data);
  });
};