/**
 * arr.filter(fn(item,index,arr),thisArg) 返回新数组--内容是符合条件的
 * https://juejin.cn/post/7037399569730109471
 */

Array.prototype.myFilter = function (fn) {
  const res = [];
  if (typeof fn !== 'function') return new TypeError('parameter1 is not a function');
  for (let i = 0; i < this.length; i++) {
    fn(this[i], i, this) && res.push(this[i]);
  }
  return res;
};

// Array.prototype.myFilter = function (fn, thisArg) {
//   const cxt = thisArg || window;
//   const res = [];
//   if (typeof fn === 'function') {
//     for (let i = 0; i < this.length; i++) {
//       fn.call(cxt, this[i], i, this) && res.push(this[i]);
//     }
//   } else {
//     return new TypeError('parameter1 is not a function');
//   }
//   return res;
// };
const arr = [1, 2, 3, 4];
console.log(
  'filter',
  arr.myFilter(i => i % 2 == 0)
);