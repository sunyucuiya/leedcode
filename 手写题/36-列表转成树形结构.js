function listToTree(items){
  const tree = [];
  const map = new Map();
  for(let each of items){
    map.set(each.id, {...each, children: []})
  }
  for(let each of items){
    if(each.parentId !==0){ //parentId=0的是父节点
      map.get(each.parentId).children.push(map.get(each.id))
      // 找每个元素的父节点，并将子节点push进去
    }else{
      tree.push(map.get(each.id))
    }
  }
  return tree;
}
// 示例数据
const items = [
  { id: 1, text: '节点1', parentId: 0 },
  { id: 2, text: '节点1_1', parentId: 1 },
  { id: 3, text: '节点1_1_1', parentId: 2 },
  { id: 4, text: '节点1_1_1', parentId: 2 },
];

// 转换成树形结构
const tree = listToTree(items);