const instanceOf = function (obj, constructor){
  //todo obj不能是null
  //todo constructor 必须是函数
  let proto = Object.getPrototypeOf(obj);
  while(proto) {
    if(proto == constructor.prototype) {
      return true;
    } 
    proto = Object.getPrototypeOf(proto);
  }
  return false;
}
console.log(instanceOf(null,Array))