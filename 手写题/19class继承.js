class People {
  constructor(name) {
    //这里定义的是实例的
    this.name = name;
    this.family = ['people'];
  }
  //这里定义的原型上的，是父类People上的方法，等同于People.prototype.greet
  greet() {
    console.log('i am ' + this.name + ',nice to meet you');
  }
}
class Women extends People {
  constructor(name) {
    super(name); // 调用父类的构造函数 等同于People.call(this，params)
  }
}

const lili = new Women('lili');
console.log(lili.name, lili.family); // lili, ['family']
lili.greet(); //i am lili,nice to meet you
console.log(lili.constructor.name);
