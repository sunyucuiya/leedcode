/**
 * 偏函数就是闭包
 * 偏函数就是将一个 n 参的函数转换成固定 x 参的函数，
 * 剩余参数（n - x）将在下次调用全部传入
 * 原理：返回包装函数，是柯里化的一种实例应用
 * 场景 bind(cxt, arg1) bind给出固定参数
 * 易错：最后也是return
 */
const partial = (fn, ...args) => {
  return (...rest)=>{
    return fn(...args, ...rest); 
  }
}
// 测试
function multi(a, b) {
  return a*b;
}
// const double = multi.bind(null, 2);
const double = partial(multi, 2);
console.log('偏函数',double(6)) //12