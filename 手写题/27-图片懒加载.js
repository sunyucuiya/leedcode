/**
 * 图片懒加载
 * 图片全部加载完成后移除事件监听；
 * 加载完的图片，从 imgList 移除；
 */
let imgList = [...document.querySelectorAll('img')];
let length = imgList.length;
const imgLazyLoad = (function() {
  let count = 0;
  return function() {
    let deleteIndexList = [];
    imgList.forEach((img, index) => {
      let rect = img.getBoundingClientReact(); //相对于视口（viewport）信息的DOM API return {top right bottom left width height}
      if(rect.top < window.innerHeight) {
        //进入窗口
        img.src = img.dataset.src; //获取data-src 添加到src来加载图片
        deleteIndexList.push(index);
        count++;
        if(count==length){
          document.removeEventListener('scroll', imgLazyLoad)
        }
      }
    })
    // 加载完的图片移除掉
    imgList = imgList.filter((i,index) => !deleteIndexList.includes(index))
  }
})() //立即执行
document.addEventListener('srcoll', imgLazyLoad)