/**
 * 【函数原型方法】手写 apply 
 *  可以改变this的指向 要判断 apply 参数可能是空数组的情况
 */
Function.prototype.applyx = function (cxt, args) {
  cxt = cxt || window;
  const fn = Symbol();
  cxt[fn] = this;
  const res = args?.length ? cxt[fn](...args) : cxt[fn]();
  delete cxt[fn];
  return res;
};

var age = 0;
var name = 'win';

var obj = {
  age: 25,
  name: 'sun',
};

var say = function (a, b) {
  console.log(this, this.age, this.name, a, b);
};
say(100, 101);
say.applyx(obj, [100, 101]);