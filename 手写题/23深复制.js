/**
 * 深复制 
 * 要解决 对象的属性也是对象的问题
 * 1 JSON安全 obj先转成JSON字符串,在转换成JSON Obj 不能正确的处理循环引用
 * 2 html dom自带的API structuredClone(value,{transfer:[]}) 
 * 3 递归遍历-具体实现原理 weakMap解决循环引用
 * https://juejin.cn/post/7278103488096829500
 */

//不能解决循环引用
const deepClone = function(obj){
  return JSON.parse(JSON.stringify(obj))
}
//不支持含有循环引用、未实现可迭代接口的自定义对象
const deepClone2 = function(obj) {
  return structuredClone(obj)
}

const deepClone3 = function (obj, visited = new WeakMap()) {
  if (visited.get(obj)) {
    return visited.get(obj);
  }
  if (typeof obj === 'object') {
    const cloneTarget = Array.isArray(obj) ? [] : {};
    visited.set(obj, cloneTarget); // 要拷贝的值 记录到map结构中 防止循环引用进入死循环
    for (let key in obj) {
      cloneTarget[key] = deepClone3(obj[key], visited); //每个属性都深拷贝
    }
    return cloneTarget;
  } else {
    return obj; //基本类型
  }
};
const obj={
  user:'haha',
  age :18,
  children: {
    user:'haha2',
    age:19
  },
  // target: obj
}
// obj.target = obj
// let cloneObj = deepClone(obj);
// let cloneObj = deepClone2(obj);

let cloneObj = deepClone3(obj);

console.log('obj',obj);
console.log('cloneObj',cloneObj);
console.log('----change----')  

cloneObj.children.user = 'cloner'

console.log('obj',obj);
console.log('cloneObj',cloneObj);

/**
 * 小知识
 * Map 与 WeakMap
 * WeakMap键是弱引用 没有引用了会垃圾回收
 */