function treeToList(tree) {
  const list = [];
  const flatten = (tree) => {
    for(let each of tree){
      if(each.children){
        flatten(each.children)
        delete each.children;
      }
      list.push(each); //在if外面
    }
  }
  flatten(tree);
  return list;
}
// 示例数据
const tree = [
  {
    id: 1,
    text: '节点1',
    parentId: 0,
    children: [
      {
        id: 2,
        text: '节点1_1',
        parentId: 1,
      },
    ],
  },
];

// 转换成树形结构
const list = treeToList(tree);