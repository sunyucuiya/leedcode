/**
 * 【函数原型方法】手写 call
 *  可以改变this的指向
 *  原理：如何指向指定的对象a？ 将方法绑定到对象a上， 然后调用
 */

Function.prototype.callx = function (context, ...args) {
  context = context || window;
  const fn = Symbol(); //创建唯一属性键名
  context[fn] = this; // 当前函数赋给上下文的属性中 来实现this的绑定
  let res = context[fn](...args);
  delete context[fn];
  return res;
};
/**
 * 以say为例 上述代码
 * 1 say调用myCall this指向say
 * 2 obj.fn = this  把say挂载到obj的方法上 这样调用say的this变成了obj 【改变了this指向】
 * 3 删除掉原来的方法
 * https://blog.csdn.net/weixin_40920953/article/details/100655515
 */
var age = 0;
var name = 'win';

var obj = {
  age: 25,
  name: 'sun',
};

var say = function (a, b) {
  console.log(this, this.age, this.name, a, b);
};
say(100, 101);
say.callx(obj, 100, 101);

//0624
Function.prototype.call0624 = function (obj, ...args) {
  obj = obj || window;
  const fn = Symbol();
  obj[fn] = this;
  const res = obj[fn](...args);
  delete obj[fn];
  return res;
}