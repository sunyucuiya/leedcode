/**
 * 节流throttle
 * 闭包 定时器：一到时间立即执行 立即就有结果 执行一次的结果
 * ? 为什么开始的时候 timer没有赋值 对比debounce开始的时候就赋值了？
 * !在一定时间里 频繁的触发只执行一次
 */
const throttle = function (fn, delay) {
  let timer;
  return function () {
    if(timer) return; //单位时间内 只执行一次
    timer = setTimeout(() => {
      fn.apply(this, arguments);
      timer = null; //执行完后 清除计时器，避免影响下一个单位执行
    },delay)
  }
}
const fn = throttle(function(){
  console.log('hi')
}, 5000
)
document.getElementById('btn').addEventListener('click',fn)

/**
 * 反思 防抖和节流 都是多次任务执行一次 
 * 防抖： 超时前不停刷新计时器 等全部触发结束后 执行
 * 节流： 超时立即执行，重新计时；时间一到就立即执行 延迟执行
 */
//简化
const _throttle = function(fn,delay) {
  let timer;
  return function() {
    if(timer) return;
    timer = setTimeout(() => {
      fn.apply(this,arguments);
      timer = null;
    }, delay)
  }
}