function People(name) {
  console.log('构造函数被调用')
  this.name = name;
  this.family = ['people']
}
People.prototype.greet = function() {
  console.log('i am '+ this.name +',nice to meet you');
}
function Women(name) {
  People.call(this,name) // 构造函数被调用
}

Women.prototype = Object.create(People.prototype) //new People(); // 组合继承中的原型链继承
Women.prototype.constructor = Women; 


const lili = new Women('lili'); 
console.log(lili.name, lili.family) // lili, ['family']
lili.greet();//i am lili,nice to meet you
console.log(lili.constructor == Women) // true