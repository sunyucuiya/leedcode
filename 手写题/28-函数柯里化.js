/**
 * 柯里化
 * 有多个参数的函数转换成使用一个参数的函数
 * 闭包+递归
 * fn(a,b,c)-->fn(a)(b)(c)
 * lodash工具库里有curry方法可以直接用 import curry from 'lodash/fp/curry'
 * https://juejin.cn/post/7272364104748531731?searchId=202311091621223E6F0CDB92BEEE73171A
 */
const curry = func => {
  let args = []; //使用闭包存储剩余参数和预设参数
  return function curried(...innerArgs) { //收集参数
    args = [...args, ...innerArgs];  //等同 args = args.concat(innerArgs);
    if (args.length >= func.length) { //func.length形参的个数
      return func(...args);
    } else {
      return curried; //继续收集剩余的参数
    }
  };
};
// 调用
const add = (a, b, c) => {
  return a + b + c;
}
let addCurry = curry(add);
addCurry(1)(2)(3)