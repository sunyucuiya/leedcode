/**
 * 实现一个对象的 flatten 方法
 */
const obj = {
  a: {
         b: 1,
         c: 2,
         d: {e: 5}
     },
  b: [1, 3, {a: 2, b: 3}],
  c: 3
 }
 function isObject(obj) {
  return typeof obj == 'object' && obj !== null;
 }
 function flatten(obj) {
  if(!isObject(obj)) return;
  let res = {};
  const dfs  = (curr, prefix) => {
    if(isObject(curr)){
      if(Array.isArray(curr)){
        curr.forEach((item,index) => {
          dfs(item,`${prefix}[${index}]`)
        })
      }else{
        for(const item in curr){
          dfs(curr[item], `${prefix}${prefix ? '.':''}${item}`)
        }
      }
    }else{
      res[prefix] = curr;
    }

  }
  dfs(obj, '');
  return res
 }
 console.log(flatten(obj))