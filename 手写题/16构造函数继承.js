function People(name) {
  this.name = name;
  this.family = ['people']
  this.sayHi = function(){
    console.log('hi, i am ' + name);
  }
}
People.prototype.greet = function() {
  console.log('nice to meet you');
}
function Women(name) {
  People.call(this,name)
}

const rose = new Women('rose');
rose.family.push('rose')
const lili = new Women('lili');
console.log(lili.name, lili.family) // lili, ['family']
lili.sayHi();//hi, i am lili
lili.greet();//Uncaught TypeError: lili.greet is not a function 不能访问父类中的方法