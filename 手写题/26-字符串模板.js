//模板字符串

// (.+?) 的含义是尽可能少地匹配一个或多个除换行符以外的任意字符，并将其作为一个捕获组
const render = (template, data) => {
  return template.replace(/\{\{(.+?)\}\}/g, (match, key) => {
    return data[key]
  })
}
/**
 * string.replace(rules, replacer) 
 * 搜索所有符合rules的匹配项，用replacer函数替换这些匹配项
 * replacer(match, key, offset, string)
 * match: 匹配到的内容,{{name}} {{age}}..
 * key: rules中的捕获组，在圆括号 () 内的部分 name age..
 * offset：原字符串开始位置索引
 * string：原字符串 我是{{name}}，年龄{{age}}，性别{{sex}}
 * replace(/\{\{(.+?)\}\}/g, function (match, key){})
 * 找到所有形如 {{key}} 的占位符，并将其内容（例如这里的 key）捕获到分组中。
 * .任意单个字符
 * +至少出现一次
 * /? 尽可能少地匹配字符,最短的一段非空文本
 */ 


//测试
let template = '我是{{name}}，年龄{{age}}，性别{{sex}}';
let person = {
    name: '布兰',
    age: 12,
    sex: '男'
}
console.log(render(template, person))
// render(template, person); // 我是布兰，年龄12，性别undefined