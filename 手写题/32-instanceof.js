/**
 * A instanceof B
 * instanceof 运算符用来检测 
 * constructor.prototype 是否存在于参数 object 的原型链上
 * 判断A是否是B的实例
 * A实例 只存了一个引用 obj.__proto__
 * B 构造函数的原型存储在prototype中
 * Object.getPrototypeOf(obj) 等价于 obj.__proto__
 * right.prototype  获取构造函数的 prototype 对象
 * 只有构造函数有prototype这个属性
 */

const myInstanceof = (left ,right) => {
  let proto = Object.getPrototypeOf(left); // 获取对象的原型
  while(proto!==null){
    if(proto == right.prototype){
      return true;
    }
    proto = Object.getPrototypeOf(proto);
  }
  return false;
}
// 测试
class People {
  constructor(){
  }
}
const p = new People()
console.log('myInstanceof',myInstanceof(p, People))