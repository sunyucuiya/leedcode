/**
 * 防抖debounce
 * 闭包 计时器：没到时间刷新计时器 用户全部活动结束后才开始执行 且执行一次
 * !频繁的触发 只执行最后一次
 */
const debounce = function (fn, delay) {
  let timer = null;
  return function () {
    if (timer) {
      clearInterval(timer);
    }
    timer = setTimeout(() => {
      fn.apply(this, arguments);
    }, delay);
  };
};
const fn = debounce(function () {
  console.log('hi');
}, 1000);
document.getElementById('btn').addEventListener('click', fn);

//简化版
const _debounce = function(fn, delay) {
  let timer = null;
  return function(...args) {
    timer && clearTimeout(timer);
    timer = setTimeout(() =>fn(...args), delay)
  }
}