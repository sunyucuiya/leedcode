/**
 * bind 改变上下文（this指向） 不同的是返回一个新函数 新函数也可以传递参数
 * 实现原理处理改变this指向 还利用了apply来合并参数
 * 还有闭包的方法 能够保留上下文
 *
 */
//手写实现 用call或者apply实现
Function.prototype.bindx = function (cxt, ...args) {
  const func = this; //当前执行的函数
  return function (...rest) {
    return func.call(cxt, ...args, ...rest); // func.apply(cxt, [...args, ...rest])
  };
};

var age = 0;
var name = 'win';

var obj = {
  age: 25,
  name: 'sun',
};

var say = function (a, b) {
  console.log(this, this.age, this.name, a, b);
};
say(100, 101);
const hi = say.bindx(obj, 100, 101);
hi();
