/**
 * 发布订阅模式：一对多的依赖关系，使得每当一个对象改变状态，则所有依赖于它的对象都会得到通知并被自动更新。 ——发布订阅模式
 * 事件总线是对发布-订阅模式的一种实现
 * 订阅on
 * 取消订阅 off
 * 发布 emit(name,once=[true|false],fn); 执行name注册的所有事件
 */
class EventEmitter {
  constructor(){
    //创建一个数据源
    this.cache = {}
  }
  on(name, fn){
    if(this.cache[name]){
      this.cache[name].push(fn)
    }else{
      this.cache[name] = [fn]
    }
  }
  off(name, fn){
    let tasks = this.cache[name];
    if(tasks){
      const index = tasks.findIndex(item => item == fn);
      index>=0 && tasks.splice(index,1);
    }
  }
  emit(name, once=false, ...args){
    if(!this.cache[name]) return;
    let task = this.cache[name];
    for(let fn of task) {
      fn(...args);
      once && this.off(name, fn)
    }
  }
}
// 测试
let eventBus = new EventEmitter()
let fn1 = function(name, age) {
	console.log(`fn1, ${name} ${age}`)
}
let fn2 = function(name, age) {
	console.log(`fn2, ${name} ${age}`)
}
eventBus.on('aaa', fn1)
eventBus.on('aaa', fn2)
// eventBus.emit('aaa', false, '布兰', 12)
eventBus.off('aaa', fn1)
// eventBus.off('aaa', fn2)

eventBus.emit('aaa', false, 'sayhai', 13)
eventBus.emit('aaa', true, 'sayhai', 13)
eventBus.emit('aaa', false,'sayhai', 13)