/**
 * 解析 URL 参数为对象
 * 正则表达式
 * 学习正则的基本用法
 * /([^?&=]+)=([^?&=]*)/g
 * /表达式/匹配规则
 */
function parseParams(url) {
  const reg = /([^?&=]+)=([^?&=]*)/g;
  return url.match(reg).reduce((params, param) => {
    let [key, value] = param.split('=');
    value = decodeURIComponent(value); // 解码
    value = /^\d+$/.test(value) ? Number(value) : value; //转成数字
    if (params.hasOwnProperty(key)) {
      // 属性已经有值
      params[key] = [].concat(params[key],value);
    } else {
      params[key] = value;
    }
    return params;
  }, {});
}
// let url = 'http:// w.com/index.html?name=haha&age=18&six=man&six=man&six=man';
// console.log(parseParams(url)); {name: haha, age: 18, six: [man, man, man]}

/**
 * /([^?&=]+)=([^?&=]*)/g
 * ()捕获组 ()=()
 * [^?&=] 不包含 ?、& 或 = 的字符
 * +至少出现一次
 * /*零个或多个
 * http:// w.com/index.html?name=haha&age=18&six=man&name=xixi&name=kiki
 * ['name=haha', 'age=18', 'six=man'...]
 * 
 * /^\d+$/ ^开头 $结尾 \d+是出现一个或多个的数字（连续）
 */