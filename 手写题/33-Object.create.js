/**
 * Object.create(proto, [propertyObject])
 * 创建新对象并指定对象的原型
 */
/**
 * 
 * @param {*} proto  新对象的原型
 * @param {*} propertyObject 属性描述对象{属性，属性描述符}
 * @returns 
 */
Object.myCreate = (proto, propertyObject = undefined) => {
  if (typeof proto !== 'object' && typeof proto !== 'function') {
    throw new TypeError('Object prototype may only be an object or null')
  }
  if (proto == null) {
    throw TypeError('Cannot convert undefined or null to object')
  }
  const obj = {};
  Object.setPrototypeOf(obj, proto);
  if (propertyObject !== undefined) {
    Object.defineProperties(obj, propertyObject);
  }
  return obj;
}