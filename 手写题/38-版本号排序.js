/**
 * 版本号排序
 * ['0.1.1', '2.3.3', '0.302.1', '4.2', '4.3.5', '4.3.4.5']。
 * 现在需要对其进行排序，
 * 排序的结果为 ['4.3.5','4.3.4.5','2.3.3','0.302.1','0.1.1']
 */

function compareVersion(v1, v2) {
  const arr1 = v1.split('.');
  const arr2 = v2.split('.');
  const len = Math.max(arr1.length, arr2.length);
  for (let i = 0; i < len; i++) {
    const num1 = Number(arr1[i]);
    const num2 = Number(arr2[i]);
    if (num1 == num2) {
      continue;
    }
    return num1-num2 // 升序 
}
}
arr = ['0.1.1', '2.3.3', '0.302.1', '4.2', '4.3.5', '4.3.4.5']
console.log(arr.sort((a, b) => compareVersion(a, b)))