/**
 * 手写jsonp 
 * 跨域解决方案
 * 创建一个 <script src=>标签 ,
 * 把那个跨域的API数据接口地址,
 * 赋值给script的src， 
 * 还要在这个地址中向服务器传递该函数名(可以通过问号传参?callback=fn)。
 */
const jsonp = ({ url, params, callback }) => {
  return new Promise(reslove => {
    const script = document.createElement('script');
    script.src = url + '?' + Object.keys(params).map(key => key+'='+params[key]).join('&') +'&callback='+callback;
    document.body.appendChild(script);
    window[callback] = data => {
      document.body.removeChild(script);
      reslove(data);
    };
  });
};
//测试方法
jsonp({
  url: 'https://jsonplaceholder.typicode.com/todos/1',
  params: {
    _id: 1
  },
  callback: 'jsonpCallback' //当外部脚本加载完成后，它应该在全局的window对象上查找一个名为jsonpCallback的函数，并执行这个函数
}).then(data => console.log(data));

function jsonpCallback(data) {
  console.log('Callback data:', data);
}


  // const getURL = () => {
  //   let src = '';
  //   for (let key in params) {
  //     if (params.hasOwnProperty(key)) {
  //       src += `${key}=${params[key]}&`;
  //     }
  //   }
  //   src += `callback=${callback}`;
  //   return `${url}?${src}`;
  // };