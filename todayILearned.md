## Reflect.ownKeys()和Object.keys()
- Reflect.ownKeys(object) 包括可枚举和不可枚举的属性名，也包括 Symbol 类型的属性名
- Object.keys(object) 不包括不可枚举属性、Symbol 类型的属性以及原型链上的属性
## js delete 操作符和 Reflect.deleteProperty
对于对象属性的删除操作，行为完全一致，Reflect.deleteProperty(object，property)，会更高阶更面向对象，保持统一性
## Reflect.has() 和 Object.prototype.hasOwnProperty() 
- Reflect.has(object，property) 会检查自身和原型链
- hasOwnProperty() 检查自身
- eslint 会报错Do not access Object.prototype method ‘hasOwnProperty‘ from target object no-prototype-builtins，不要使用对象原型上的方法，因为原型的方法可能会被重写
## webpack的bundle和chunk
bundle打包最终产出的资源文件
chunk打包过程中的中间产物 多个module合并，分割代码就是合理的划分chunk 如optimization:{runtimeChunk, splictChunks}配置

## 两种模块格式 CommonJs 和 EsModule
- CommonJs：
  -  require()， module.exports， exports = {} 目标是模块对象
  -  require运行时加载，同步加载 
  -  输出拷贝
  -  循环引用时 加载到的直接输出
  -  应用：加载本地资源
- EsModule：
  -  import， export default， export {} 目标是数据
  -  import编译时输出接口 异步加载， 不能写在块级、条件判断语句中
  -  输出引用 
  -  避免循环引用
  -  应用：加载网络资源
## vue3 响应式 ref reactive toRefs toRef ref标签属性
- reactive不能解构，不能直接替换整个对象，可以修改属性不影响响应式  
- toRefs(reactive_obj) 只能接收rective对象,**解构**的时候用， toRef(obj, property)单个属性
- ref标签属性 子组件的引用存储在同名的ref()中 引用dom需等待挂载后才获取到dom
```html
<p ref="p">hello</p> 
```
```js
const p = ref() //setup()中的引用,需等待挂载后才获取到dom
```
## setup（vue3）
`setup`比`beforeCreated`执行的早，所以不能访问到`data`中的方法（如果在vue3中用vue2的语法的话）    
`setup()` 返回页面内容的话会覆盖`template`中的内容  
在`script`标签添加`setup`会自动识别变量和方法 不用`return`，但是修改组件名会比较麻烦  
👋setup()在props解析之后，组件beforeCreated之前  
👋setup的参数，第一个是props，第二个是SetupContext包含`{attrs, slots, emit, expose}`,props在使用的时候用`toRefs(props)`包裹一下再解构  
- attrs
- slots 
- emit 触发父级的方法  
```js
//子组件
setup(props, {emit}){
  emit('changeSome', '123') //emit(name,playload)
}
//父组件
setup(){
  const changeSome=(data) => {}
}
```
- expose 向父级暴露自己的方法，子组件需要用ref属性
```js
//子组件
setup(props, {expose}){
  expose({func1,func2}) //向父级暴露自己的方法
}
//夫组件
setup(){
  const subComponent = ref()
  subComponent.value?.func1()//调用子组件的方法
  return () => (
    <sub-component ref={subComponent} />
  )
}
```

## js中为什么不推荐使用`argument`和`callee`
 * 主要原因：`arguments`和`callee`是早期非标准特性和习惯用法，现在有了标准化的新特性，尽量避免使用
 * `arguments` 类数组对象，可以访问到调用时传递的参数，隐式处理；类数组不是真数组不能用数组的方法
 * `callee`是`arguments`对象的一个属性它引用的是当前正在执行的函数本身，箭头函数中不能用
 * 在严格模式下，访问 `arguments.callee` 会抛出错误
  
## `findIndex()`和`indexOf()`
1. 都是用来查找数组中元素的索引位置的方法，返回匹配到的第一个元素的索引位置，没有则为`-1`；
2. `indexOf(item)`是原生` Array` 对象提供的方法，`findIndex(callback)``ES6` 新增的数组方法；
3. 需要根据简单值查找时，可以使用 `indexOf()`；
而当你需要基于复杂条件（例如对象属性、函数引用或其他自定义逻辑）查找时，则更适合使用 `findIndex()`      

总结： 查的是基本类型的用indexOf();查的是引用类型或者带复杂逻辑的用findindex
##   for循环总结
> 虽然有的能实现相同的效果，但是这里对自己规范一下  
### for
 * 支持break，continue控制语句
 * 
### for...of 
 * 数组，Set、Map、生成器函数（实现了迭代器接口）
 * 支持break，continue控制语句
 * 遍历数组不需要下标可以用这个 for(let item of arr)
 * 
### for await of
 * 遍历异步可迭代对象，该语句只能在一个async function 内部使用
 * 
### for...in
 * 遍历对象，不保证顺序
 * 属性以及继承自原型链上的可枚举属性，Symbol不能被遍历
 * 
### Array.prototype.forEach((item,index,arr)=>{})
 * 遍历数组，纯遍历操作，有更多的参数可用，遍历数组需要下标可以用这个
 * 不支持break，continue控制语句
 * 其他的遍历数组原生方法：map(), reduce(), filter(), every(), some()
### 性能比较
 * for > for of > forEach > map > for in