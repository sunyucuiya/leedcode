/**
 * 异步操作的同步化表达
 */
//简单应用
const loadUI = function* () {
  showLoadingScreen();
  yield loadUIDataAsynchronously();
  hideLoadingScreen();
}
const load_it = loadUI();
load_it.next(); // 调用异步操作
load_it.next(); //执行回调

/**
 *  Generator 函数部署 Ajax 操作
 */

