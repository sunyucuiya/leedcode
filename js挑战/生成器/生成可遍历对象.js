/**
 * 利用生成器，得到可遍历对象 for...of
 * js原生对象没有迭代器  obj is not iterable
 * todo: 生成器对象可以使用替代next()：（1）扩展运算符（...）、（2）解构赋值 （3）Array.from方法
 */

/**
 *! 方案一： 通过 Generator 函数为它加上这个接口
 */
function* objectEntries(obj) {
  const props = Reflect.ownKeys(obj);
  for(let prop of props) {
    yield [prop, obj[prop]];
  }
}

let james = { first: 'James', last: 'Bond' };
for(let [prop, value] of objectEntries(james)) {
  console.log(`${prop}: ${value}`);
}

/**
 *! 方案二： 将 Generator 函数加到对象的Symbol.iterator属性上面
 * ?Reflect.ownKeys 得到可遍历和不可遍历（符号）属性
 * ?Object.keys 得到可遍历属性
 */

function* objectEntries() {
  let propKeys = Object.keys(this); //this的指向，指向jane
  //todo 不能用Reflect.ownKeys(this) Symbol属性也会拿到，导致后面遍历的时候会报错

  for (let propKey of propKeys) {
    yield [propKey, this[propKey]];
  }
}

let jane = { first: 'Jane', last: 'Doe' };

jane[Symbol.iterator] = objectEntries;


for (let [key, value] of jane) {
  console.log(`${key}: ${value}`);
}

