/**
 * 请你编写一个函数 argumentsLength，返回传递给该函数的参数数量。
 */
var argumentsLength = function(...args) {
  return args.length;
};
/**
 * 涉及到的知识点
 * ...args形式是【剩余参数】
 * function(...args) 函数参数是以**剩余参数**的形式传递的，可以接受任意数量的参数。
 * 所有的参数会收集到 args 数组中。没有参数则是空数组。
 * 
 *!【剩余参数】不等于 【展开运算符】
 * 【展开运算符】 用于展开数组或对象； obj={...obj1,...obj2}
 * 
 * 其他 arguments是js的一个类数组对象【参数对象】，包含了函数调用时传递的所有参数
 * 但是他没有Array的内置方法
 */