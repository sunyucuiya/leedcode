/**
[f(x)， g(x)， h(x)] 的 复合函数 为 fn(x) = f(g(h(x))) 
 */

//循环解决
var compose = function (functions) {
  return function (x) {
    let res = x;
    for (let i = functions.length - 1; i >= 0; i--) {
      res = functions[i](res);
    }
    return res;
  };
};

//保留this
var compose = function (functions,ctx) {
  return function (x) {
    let res = x;
    for (let i = functions.length - 1; i >= 0; i--) {
      res = functions[i].call(ctx, res);
    }
    return res;
  };
};

//用reduce
var compose = function (functions) {
  return function (x) {
    return functions.reduceRight((pre, curr) => curr(pre), x);
  };
};

/**
 * 复合函数相关知识点
 * 复合函数/组合函数 是一种函数式编程
 * 一个函数的输出是另一个函数的输入（链式？）
 *?拓展 考虑处理 **this** 上下文： 在调用组合函数时使用 call 或 apply 方法
 *?res = functions[i].call(this, res);
 */

const f = (x) => x+1;
const g = (x) => x*x;
const h = (x) => x*2;


// const res = compose([obj.f,obj.g,obj.h], obj)(4); //NaN this指向不是obj
// const res = compose([f,g,h])(4); //这里的this是window
console.log('组合函数',res); //65