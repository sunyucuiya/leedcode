/**
给定一个函数 fn ，它返回一个新的函数，返回的函数与原始函数完全相同，只不过它确保 fn 最多被调用一次。
第一次调用返回的函数时，它应该返回与 fn 相同的结果。
第一次后的每次调用，它应该返回 undefined 。
 */

var once = function (fn) {
  let used = false;
  return function (...args) {
    if (!used) {
      used = true;
      return fn(...args);
    }
  };
};
/**
 * 相关知识--函数式编程
 * 【高阶函数】修改或扩展函数行为的函数
 * 场景： 节流、防抖、记忆化、时间限制
 */