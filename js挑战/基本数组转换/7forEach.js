/**
 * 手写数组原型方法 forEach
 * 原生 arr.forEach(function(item,index,arr),thisArg)\
 * 两个参数 1-函数(每一项，下标，数组本身)2-上下文
 */

Array.prototype.myForEach = function (fn) {
  if (typeof fn !== 'function') return new TypeError('parameter1 is not a function');
  for (let i = 0; i < this.length; i++) {
    fn(this[i], i, this);
  }
};

// Array.prototype.myForEach = function (fn, thisArg) {
//   const cxt = thisArg || window;
//   if (typeof fn === 'function') {
//     for (let i = 0; i < this.length; i++) {
//       fn.call(cxt, this[i], i, this);
//     }
//   } else {
//     return new TypeError('parameter1 is not a function');
//   }
// };
const arr = [1, 2, 3, 4];
arr.myForEach(item => console.log(item * 2));
