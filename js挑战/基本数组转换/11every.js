/**
 * arr.every(fn(item,index,arr),thisArg) 返回boolean 只要有一个不满足就false 全满足才true
 * https://juejin.cn/post/7038063486827495461
 */

Array.prototype.myEvery = function (fn) {
  const res = true;
  if (typeof fn !== 'function') return new TypeError('parameter1 is not a function');
  for (let i = 0; i < this.length; i++) {
    if (!fn(this[i], i, this)) {
      return false;
    }
  }
  return res;
};
// Array.prototype.myEvery = function (fn, thisArg) {
//   const cxt = thisArg || window;
//   const res = true;
//   if (typeof fn === 'function') {
//     for (let i = 0; i < this.length; i++) {
//       if (!fn.call(cxt, this[i], i, this)) {
//         return false;
//       }
//     }
//   } else {
//     return new TypeError('parameter1 is not a function');
//   }
//   return res;
// };
const arr = [1, 2, 3, 4];
console.log('every',arr.myEvery(i => i % 2 == 0));
