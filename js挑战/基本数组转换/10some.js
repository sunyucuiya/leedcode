/**
 * arr.some(fn(item,index,arr),thisArg) 返回boolean 1存在一个元素满足条件true 2都不满足返回false
 * https://juejin.cn/post/7038063486827495461
 */

//返回值 true/false
Array.prototype.mySome = function (fn) {
  const res = false;
  if (typeof fn !== 'function') return new TypeError('parameter1 is not a function');
  for (let i = 0; i < this.length; i++) {
    if (fn(this[i], i, this)) {
      return true;
    }
  }
  return res;
};
// Array.prototype.mySome = function (fn, thisArg) {
//   const cxt = thisArg || window;
//   const res = false;
//   if (typeof fn === 'function') {
//     for (let i = 0; i < this.length; i++) {
//       if (fn.call(cxt, this[i], i, this)) {
//         return true;
//       }
//     }
//   } else {
//     return new TypeError('parameter1 is not a function');
//   }
//   return res;
// };
const arr = [1, 2, 3, 4];
console.log('some',arr.mySome(i => i % 2 == 0));