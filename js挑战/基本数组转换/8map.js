/**
 * arr.map(fn(item,index,arr),thisArg) 返回新数组
 * 跟forEach的逻辑一样 只是返回的是一个新数组
 * https://juejin.cn/post/7036506894449508365
 * 
 * fn(item,index,arr) 回调函数
 */

//简化版本
//!返回新数组
Array.prototype.myMap = function (fn) {
  const res = [];
  if (typeof fn !== 'function') return new TypeError('Parameter1 is not a function');
  for (let i = 0; i < this.length; i++) {
    res.push(fn(this[i], i, this));
  }
  return res;
};

// Array.prototype.myMap = function (fn, thisArg) {
//   const res = [];
//   const cxt = thisArg || window;
//   if (typeof fn == 'function') {
//     for (let i = 0; i < this.length; i++) {
//       res.push(fn.call(cxt, this[i], i, this));
//     }
//   } else {
//     return new TypeError('Parameter1 is not a function');
//   }
//   return res;
// };

const arr = [1, 2, 3, 4];
console.log(arr.myMap(item => item + 1));