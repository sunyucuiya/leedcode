/**
 * arr.reduce(fn(pre, curr, index, arr),init) 
 * 返回一个遍历完数组后计算的值
 */
//fn(pre, item, index, arr)
Array.prototype.myReduce = function (fn, init) {
  let res = init || this[0]; //当前数组的第一个元素
  let i = init ? 0 : 1;
  if (typeof fn !== 'function')  throw new TypeError('parameter1 is not a function');
  for (i; i < this.length; i++) {
    res = fn(res, this[i], i, this);
  }
  return res;
};
let arr = [1, 2, 3];
console.log(arr.myReduce((pre, curr) => pre + curr));
console.log(arr.myReduce((pre, curr) => pre + curr, 6));
 
 /**
  * reduce实用技巧
  * reduce有遍历，处理的时候可以拿到上一次的值，可以做一些累计，分类，聚合的逻辑
  */