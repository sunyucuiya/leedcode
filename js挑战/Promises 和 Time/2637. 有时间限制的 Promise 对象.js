/**
请你编写一个函数，它接受一个异步函数 fn 和一个以毫秒为单位的时间 t。
它应根据限时函数返回一个有 限时 效果的函数。函数 fn 接受提供给 限时 函数的参数。
限时 函数应遵循以下规则：
如果 fn 在 t 毫秒的时间限制内完成，限时 函数应返回结果。
如果 fn 的执行超过时间限制，限时 函数应拒绝并返回字符串 "Time Limit Exceeded" 。
 
示例 1：

输入：
fn = async (n) => { 
  await new Promise(res => setTimeout(res, 100)); 
  return n * n; 
}
inputs = [5]
t = 50
输出：{"rejected":"Time Limit Exceeded","time":50}
解释：
const limited = timeLimit(fn, t)
const start = performance.now()
let result;
try {
   const res = await limited(...inputs)
   result = {"resolved": res, "time": Math.floor(performance.now() - start)};
} catch (err) {
   result = {"rejected": err, "time": Math.floor(performance.now() - start)};
}
console.log(result) // 输出结果

提供的函数设置在 100ms 后执行完成，但是设置的超时时间为 50ms，所以在 t=50ms 时拒绝因为达到了超时时间。
 */

/**
 * 状态改变后不会再变 
 * 使用setTimeout 来实现超时拒绝，但是需要clearTimeout
 */
var timeLimit = function (fn, t) {
  return async function (...args) {
    return new Promise((res, rej) => {
      let timer = setTimeout(() => rej('Time Limit Exceeded'), t); //超时拒绝
      fn(...args)
        .then(res)
        .catch(rej)
        .finally(() => clearTimeout(timer));
    });
  };
};
//使用promise.race
var timeLimit = function (fn, t) {
  return async function (...args) {
    const timeLimitPromise = new Promise((res, rej) => {
      setTimeout(()=>rej('Time Limit Exceeded'),t)
    })
    const reslutPromise = fn(...args);
    return Promise.race([timeLimitPromise,reslutPromise])
  };
};


/**
 * 时间限制用例
 * （1）长时间运行的过程
 * （2）通知用户失败
 */