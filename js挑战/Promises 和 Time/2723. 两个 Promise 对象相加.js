/**
给定两个 promise 对象 promise1 和 promise2，返回一个新的 promise。
promise1 和 promise2 都会被解析为一个数字。
返回的 Promise 应该解析为这两个数字的和。
 */
var addTwoPromises = async function(promise1, promise2) {
  return (await promise1) + (await promise2);
};
var addTwoPromises = async function(promise1, promise2) {
  const [res1, res2] = await Promise.all([promise1, promise2]);
  return res1+res2;
};

/**
 * promise相关知识回顾
 * Promise是异步对象 new Promise(resolve,reject)
 * 状态：pending  fulfilled rejected
 * async/await 把异步包装成同步代码；
 * 相关的方法： 
 * （1）Promise.all([p1,p2,p3])  
 *  同时处理；等待所有promise都解析（完成/失败）后才返回；有一个rejected就返回rejected
 * （2）Promise.race()
 * 如何捕捉错误？ 【异步/同步】
 *   (1)用try..catch 捕捉 async/await 中的错误
 *   try..catch捕捉不了promise的错误，因为try..catch只能捕捉同步代码的错误
 *   (2)Promise用promise.catch 方法来捕捉
 */