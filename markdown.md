https://markdown.com.cn/basic-syntax/
# H1
## H2
### H3
### 加粗
**bold text**  
*italicized text*
### 引用
> blockquote  
### 列表
1. First item
2. Second item
3. Third item
- First item
- Second item
- Third item  
### 代码高亮
  `code`  
---
[title](https://www.example.com)  
![alt text](image.jpg)
### 表格
| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |  
### 代码块
```
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```  
Here's a sentence with a footnote.  
 [^1][^1]: This is the footnote.  
 ### My Great Heading {#custom-id}  
 term
: definition

~~The world is flat.~~  
- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media    

This **word** is bold. This <em>word</em> is italic.  
<https://markdown.com.cn>
<fake@example.com>  
***
theme: serene-rose