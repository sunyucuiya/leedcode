var intersect = function(nums1, nums2) {
   const map = {};
   const res = [];
   for(const num of nums1){
    if(map[num]){
        map[num]++
    }else{
        map[num]=1
    }
   }
   for(const num of nums2){
    if(map[num]){
        res.push(num);
        map[num]--
    }
   }
   return res;
};