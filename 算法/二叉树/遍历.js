/**
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */

/**
 * 前序遍历-递归 
 */
const preOrder = (root, reslut=[]) => {
  if(!root) return reslut;
  reslut.push(root.val);
  preOrder(root.left, reslut);
  preOrder(root.right, reslut);
  return reslut;
}

/**
 * 中序遍历-递归
 */
const inOrder = (root, result=[]) => {
  if(!root) return result;
  inOrder(root.left, result);
  result.push(root.val);
  inOrder(root.right, result);
  return result;
}

/**
 * 后序遍历-递归
 */
const postOrder = (root, result=[]) => {
  if(!root) return result;
  postOrder(root.left, result);
  postOrder(root.right, result);
  result.push(root.val);
  return result;
}