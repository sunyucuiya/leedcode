/**
 * 构建二叉树
 * （中序遍历可以提供关于根节点在左右子树中的位置的信息）
 * 前序+中序
 * 后序+中序
 * 层序
 */
const preOrderList = [1, 2, 4, 5, 3];
const inOrderList = [4, 2, 5, 1, 3];
const postOrderList = [4, 5, 2, 3, 1];
const levelOrderList = [1, 2, 3, 4, 5];

function TreeNode(val) {
  this.val = val;
  this.left = this.right = null;
}

/**
 * 根据 前序+中序 构建二叉树
 */
function buildTreeByPreAndIn(preOrderList, inOrderList) {
  if(preOrderList.length === 0 || inOrderList.length == 0 ) {
    return null;
  }

  const root = new TreeNode(preOrderList[0]);
  const index = inOrderList.indexOf(preOrderList[0]);

  root.left = buildTreeByPreAndIn(preOrderList.slice(1, index+1),)

}
