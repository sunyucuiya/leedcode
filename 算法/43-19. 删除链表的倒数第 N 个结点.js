/**
 * 链表的移动到指定位置
 * 快慢指针，快指针走n步之后 快慢一起走
 */
var removeNthFromEnd = function(head, n) {
    
    let dummyHead = new ListNode(0,head);
    let slow = dummyHead;
    let fast = dummyHead;
    while(n--){
        fast = fast.next;
    }
    while(fast.next){
        fast = fast.next;
        slow = slow.next;
    }
    slow.next = slow.next.next;
    return dummyHead.next;
};