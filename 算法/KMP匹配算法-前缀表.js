/**
 * 前缀表用来回退用的
 * 匹配到不相等的 跳转到当前的前一个所对应的的前缀表记录的位置
 * @param {*} needle  模式串
 */
const getNext = (needle) => {
  let next = [];
  let j = 0;
  next.push(j);
  for (let i = 0; i < needle.length; i++) {
    while (j > 0 && needle[j] !== needle[i]) {
      j = next[j - 1];
    }
    if (needle[j] = needle[i]) {
      j++;
    }
    next.push(j)
  }
  return next;
}