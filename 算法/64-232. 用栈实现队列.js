/**
 * js用什么数据结构模拟栈和队列呢
 * 栈：数组
 */

var MyQueue = function() {
  this.stackIn = [];
  this.stackOut = []
};

/** 
 * @param {number} x
 * @return {void}
 */
MyQueue.prototype.push = function(x) {
  this.stackIn.push(x);
};

/**
 * @return {number}
 */
MyQueue.prototype.pop = function() {
  if(this.stackOut.length){
    return this.stackOut.pop();
  }
  while(this.stackIn.length){
    this.stackOut.push(this.stackIn.pop())
  }
  return this.stackOut.pop()
};

/**
 * 得到第一个
 * @return {number}
 */
MyQueue.prototype.peek = function() {
  const item = this.pop();
  this.stackOut.push(item);
  return item
};

/**
 * @return {boolean}
 */
MyQueue.prototype.empty = function() {
 return ! this.stackIn.length && !this.stackOut.length;
};

/**
 * Your MyQueue object will be instantiated and called as such:
 * var obj = new MyQueue()
 * obj.push(x)
 * var param_2 = obj.pop()
 * var param_3 = obj.peek()
 * var param_4 = obj.empty()
 */