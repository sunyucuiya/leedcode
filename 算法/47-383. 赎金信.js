var canConstruct = function(ransomNote, magazine) {
    let hash = new Array(26).fill(0);
    for(let i=0;i<magazine.length;i++){
        hash[magazine[i].charCodeAt() - 'a'.charCodeAt()]++;
    }
    for(let i =0;i<ransomNote.length;i++){
        hash[ransomNote[i].charCodeAt() - 'a'.charCodeAt()]--;
    }
    for(let i=0;i<26;i++){
        if(hash[i]<0) return false;
    }
    return true;
};