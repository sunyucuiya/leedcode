/**
1005. K 次取反后最大化的数组和
给你一个整数数组 nums 和一个整数 k ，按以下方法修改该数组：
选择某个下标 i 并将 nums[i] 替换为 -nums[i] 。
重复这个过程恰好 k 次。可以多次选择同一个下标 i 。
以这种方式修改数组后，返回数组 可能的最大和 。
输入：nums = [4,2,3], k = 1
输出：5
解释：选择下标 1 ，nums 变为 [4,-2,3] 。
 */

/**
 * 全正数 只反转最小的正数
 * 有负数 从小（绝对值大）的开始反转负数
 * k没消耗完： 重新排序，做正数的处理，可以和正数处理合并
 */
var largestSumAfterKNegations = function(nums, k) {
  nums.sort((a,b)=>a-b);
  for(let i=0;i<nums.length;i++) {
    if(nums[i]<0 && k>0) {
      nums[i] = -nums[i];
      k--;
    }
  }
  nums.sort((a,b)=>a-b);
  return nums.reduce((a,b)=>a+b)- (k%2)*nums[0]*2;
};
const nums = [4,2,3], k = 1;
console.log(' K 次取反后最大化的数组和', largestSumAfterKNegations(nums, k)); //5