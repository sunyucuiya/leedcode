/**
45.跳跃游戏 II
给定一个长度为 n 的 0 索引整数数组 nums。初始位置为 nums[0]。
每个元素 nums[i] 表示从索引 i 向前跳转的最大长度。
换句话说，如果你在 nums[i] 处，你可以跳转到任意 nums[i + j] 处:
0 <= j <= nums[i] 
i + j < n
返回到达 nums[n - 1] 的最小跳跃次数。生成的测试用例可以到达 nums[n - 1]。

输入: nums = [2,3,1,1,4]
输出: 2
解释: 跳到最后一个位置的最小跳跃数是 2。
     从下标为 0 跳到下标为 1 的位置，跳 1 步，然后跳 3 步到达数组的最后一个位置。
 */

/**
 * 什么时候步数才一定要加一呢?
 * 最小的扩大覆盖范围
 * i ~ nums.length-2 
 * 不断的去找最大cover
 * i 到了edge 步数加一;edge更新为下一个over
 */
var jump = function(nums) {
  let cover = 0
  let edge = 0
  let res = 0;
  for(let i=0;i<nums.length-1;i++) { //nums.length-1 是因为最后一步不用跳了,不需要更新步数了
    cover = Math.max(i+nums[i], cover)
    if(i == edge) { //到覆盖范围边界，步数加一
      edge = cover;
      res++;
    }
  }
  return res;
};
const nums = [2,3,1,1,4];
console.log('跳跃游戏 II', jump(nums)); //2