/**
跳跃游戏
给你一个非负整数数组 nums ，你最初位于数组的 第一个下标 。数组中的每个元素代表你在该位置可以跳跃的最大长度。
判断你是否能够到达最后一个下标，如果可以，返回 true ；否则，返回 false 。

输入：nums = [2,3,1,1,4]
输出：true
解释：可以先跳 1 步，从下标 0 到达下标 1, 然后再从下标 1 跳 3 步到达最后一个下标。
 */

/**
 * 每次取最大跳跃步数
 * cover = Math.max(i+nums[i], cover)
 * i+nums[i] 加上了到开始的距离
 * i cover
 * 0 2
 * 1 1+3  这里就可以到终点了 cover>=nums.length-1
 * 2 4
 * 3 4
 * 4 16
 */
var canJump = function(nums) {
  if(nums.length===1) return true;
  let cover = 0
  for(let i=0;i<=cover;i++) {
    cover = Math.max(i+nums[i], cover)
    if(cover>=nums.length-1) return true;
  }
  return false;
};
// const nums = [2,3,1,1,4]; //true
const nums = [3,2,1,0,4]; //false
console.log('跳跃游戏', canJump(nums)); //true