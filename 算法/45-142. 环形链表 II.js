/**
 * 1. 是否有环
 * 快慢指针： 快指针每次移动两步，慢指针每次移动一步，
 * 如果有环：快指针慢指针先后进入环中，快指针追赶慢指针最终会相遇
 * 如果没有环：快慢指针永远不会相遇
 * 2. 找到环的位置
 * slow= x+y
 * fast = x+n(y+z)+y
 * 2(x+y) = x+n(y+z)+y
 * x=n(y+z)-y
 * x=(n-1)(y+z)+z
 * y: 直到相遇 慢指针移动的距离
 * z: 快指针距离入口的距离===head到入口的距离
 * 慢指针一定在第一圈就被追赶上
 */
var detectCycle = function(head) {
    let slow = head;
    let fast = head;
    while(fast&&fast.next){
        fast = fast.next.next;
        slow = slow.next;
        if(slow==fast){
            let index1 = fast;
            let index2 = head;
            while(index1!==index2){
                index1 = index1.next;
                index2 = index2.next;
            }
            return index1
        }
    }
    return null
};