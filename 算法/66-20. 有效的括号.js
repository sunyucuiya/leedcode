/**
 * 所有左侧的进栈（记录对应的右侧），遇到右侧进行比较
 */
var isValid = function(s) {
  let stack = [];
  for(let i=0;i<s.length;i++){
    if(s[i]=='('){
      stack.push(')')
    }else
    if(s[i]=='['){
      stack.push(']')
    }else
    if(s[i]=='{'){
      stack.push('}')
    }else if(s[i]!==stack.pop()){
      return false
    }
  }
  return stack.length==0
};