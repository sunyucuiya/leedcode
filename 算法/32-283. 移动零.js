
/**
给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
请注意 ，必须在不复制数组的情况下原地对数组进行操作。
输入: nums = [0,1,0,3,12]
输出: [1,3,12,0,0]
 */

/**
 * 双指针
 * fast for中的i
 * slow 停在0的位置不动，让快指针选中的非零元素赋值给 slow对应位置
 * 最后处理剩余元素，从slow位置往后都赋值0
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var moveZeroes = function (nums) {
  let slow = 0;
  for (let i = 0; i < nums.length; i++) {
    if (nums[i] !== 0) {
      nums[slow++] = nums[i];
    }
  }
  // 剩余位置全部设置0
  while (slow < nums.length) {
    nums[slow++] = 0;
  }
  return nums;
};