
var partition = function (s) {
  let res = [];
  let path = [];
  var backTracking = (s, index) => {
    if(index>=s.length){ //?? 切割线切到了字符串最后面
      res.push(path.slice());
      return;
    }
    for (let i = index; i < s.length; i++) {
      if(!isPalindrome(s,index,i)) continue;
      path.push(s.slice(index,i+1));
      backTracking(s, i + 1);
      path.pop();
    }
  }
  backTracking(s, 0)
  return res;
};
var isPalindrome = (str, left, right) =>{
  while(left<right){
    if(str[left]!==str[right]) return false;
    left++;
    right--;
  }
  return true;
}
