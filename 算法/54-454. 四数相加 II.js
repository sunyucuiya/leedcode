/**
 * 先合并两个 在判断另外两个
 * 记录出现的次数
 */
var fourSumCount = function(nums1, nums2, nums3, nums4) {
    const map = {};
    let res = 0;
    for(const n1 of nums1){
        for(const n2 of nums2){
            const sum = n1+n2;
            map[sum] = 1 + (map[sum]||0);//注意优先级
        }
    }
    for(const n3 of nums3){
        for(const n4 of nums4){
            const sum = n3+n4;
            res = res + (map[0-sum]||0);
        }
    }
    return res;
};