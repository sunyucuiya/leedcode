/**
递增子序列
给你一个整数数组 nums ，找出并返回所有该数组中不同的递增子序列，递增子序列中 至少有两个元素 。
你可以按 任意顺序 返回答案。
数组中可能含有重复元素，如出现两个整数相等，也可以视作递增序列的一种特殊情况。
输入：nums = [4,7,6,7]
输出：[ [ 4, 7 ], [ 4, 7, 7 ], [ 4, 6 ], [ 4, 6, 7 ], [ 7, 7 ], [ 6, 7 ] ]
 */

/**
 * !原数组不能排序
 * 【两个整数相等，视作递增序列的一种特殊情况】
 * used[nums[i]] 同层nums[i]已经使用过---忽略； 
 * 但是同枝可以使用，used的作用范围只有层 所以不在回溯后恢复状态
 */
var findSubsequences = function(nums) {
  const res = [];
  const path = [];
  
  const backTrack = (index) => {
    path.length > 1 && res.push(path.slice()); //!其他深度也要收集 不能return；
    let used = {}; //进入新的一层 重置used
    for(let i=index;i<nums.length;i++) {
      if(used[nums[i]]==true) continue;
      if(path.length > 0 && nums[i] < path[path.length - 1])  continue;
      
      path.push(nums[i]);
      used[nums[i]]=true; //!只关注同层的使用情况
      backTrack(i+1);
      path.pop();
    }
  }
  backTrack(0);
  return res;
};
const nums = [4,7,6,7];
console.log('递增子序列',findSubsequences(nums));