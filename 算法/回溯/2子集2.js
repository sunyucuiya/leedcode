/**
子集II
给你一个整数数组 nums ，其中可能**包含重复元素**，请你返回该数组所有可能的 子集（幂集）。
解集 不能 包含重复的子集。返回的解集中，子集可以按 任意顺序 排列。
示例 1：
输入：nums = [1,2,2]
输出：[[],[1],[1,2],[1,2,2],[2],[2,2]]
 */

/**
 * 数组有重复元素，可以选择，但是结果集不能重复
 * 同层的不能重复 前提：一定要排序！！
 * used数组  nums[i-1]=nums[i] && used[i-1]==false
 */
var subsetsWithDup = function(nums) {
  const res = [];
  const path = [];
  const used = Array(nums.length).fill(false);
  nums.sort((a,b) => a-b)
  const backTrack = (index) => {
    res.push(path.slice());
    for(let i = index;i<nums.length; i++) {
      if(i>0 && nums[i-1]==nums[i] && used[i-1]==false) continue;
      path.push(nums[i]);
      used[i]=true;
      backTrack(i+1);
      used[i]=false;
      path.pop();
    }
  }
  backTrack(0);
  return res;
};

const nums = [1,2,2];
console.log('子集2',subsetsWithDup(nums));