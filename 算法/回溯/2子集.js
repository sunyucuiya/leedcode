/**
子集 没有终止条件和剪枝 收集所有结果！
给你一个整数数组 nums ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。
解集 **不能** 包含重复的子集。你可以按 任意顺序 返回解集。
输入：nums = [1,2,3]
输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]

输入：nums = [0]
输出：[[],[0]]
 */
/**
 * 方案 子集就是不做任何限制的组合
 * 不写终止条件,没有剪枝
 */
var subsets = function(nums) {
  const res = [];
  const path = [];
  const backTrack = (index) =>{
    res.push(path.slice());
    for(let i=index;i<nums.length;i++) {
      path.push(nums[i]);
      backTrack(i+1);
      path.pop();
    }
  }
  backTrack(0);
  return res;
}
/**
 *  方案：收集每层的组合结果？？k=0...nums.length
 * 【无序】顺序不同算同一个结果 需要 index
 * 【终止条件??】层级不限制 k最大是nums.length;
 *  
 */
var subsets0 = function(nums) {
  const res = [];
  const path = [];
  const n= nums.length;
  const backTrack = (k,index) => {
    if(path.length == k) {
      res.push(path.slice());
      return;
    }
    for(let i=index; i<n-k+path.length+1; i++) {
      path.push(nums[i]);
      backTrack(k,i+1);
      path.pop();
    }
  }
  for(let k=0; k<=n; k++) {
    backTrack(k, 0);
  }
  return res;
};
const nums = [1,2,3];
console.log('子集',subsets(nums));