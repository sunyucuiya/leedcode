/**
 * 
 */
var findAnagrams = function(s, p) {
    const res = []
    const hash = new Array(26).fill(0);
    for(let i of p){
        hash[i.charCodeAt() - 'a'.charCodeAt()]++
    }
    for(let i=0;i<=s.length-p.length;i++){
        let h = hash.slice();
        let flag = true;
        for(let j = i;j<i+p.length;j++){
            h[s[j].charCodeAt() - 'a'.charCodeAt()]--
        }
        for(let x = 0;x<26;x++){
            if(h[x]!==0) flag=false;
        }
        if(flag){
            res.push(i)
        }
    }
    return res;
};