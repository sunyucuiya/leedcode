/**
如果剩余字符少于 k 个，
  则将剩余字符全部反转。
如果剩余字符小于 2k 但大于或等于 k 个，
  则反转前 k 个字符，其余字符保持原样。
 */
var reverseStr = function (s, k) {
  const len = s.length;
  let arr = [...s];
  for (let i = 0; i < len ; i += 2 * k) {
    let left = i-1;
    let right = i+k > len ? len: i+k; //规则就看最后一个2k返回  
    while(++left < --right){ //加加减减合并操作
      [arr[left],arr[right]] = [arr[right],arr[left]];
     
    }
  }
  return arr.join('');
};