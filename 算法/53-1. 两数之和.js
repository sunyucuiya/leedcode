/**
给定一个整数数组 nums 和一个整数目标值 target，
请你在该数组中找出和为目标值 target的那两个整数，
并返回它们的数组下标。
你可以假设每种输入只会对应一个答案。
但是，数组中同一个元素在答案里不能重复出现。
你可以按任意顺序返回答案。
示例 1：
输入：nums = [2,7,11,15], target = 9
输出：[0,1]
解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
 */

/**
 * map: {arrValue: index}
 * map[nums[i]] = i 遍历记录，
 * 记录的过程中检查两个另外一个加数已经记录过 说明找到了两个加数
 * 哈希表来记录是否遍历过 key:nums value:index
 */
var twoSum = function (nums, target) {
  const map = {};
  for (let i = 0; i < nums.length; i++) {
    if (map[target - nums[i]] !== undefined) {
      //判断条件必须这样 因为下标0可能会导致错误
      return [i, map[target - nums[i]]];
    } else {
      map[nums[i]] = i;
    }
  }
  return [];
};