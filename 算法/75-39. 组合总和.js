var combinationSum2 = function (candidates, target) {
  let res = [];
  let path = [];
  candidates.sort((a, b) => a - b);
  var backTracking = (n, index) => {
    path.sort((a, b) => a - b)
    let sum = path.reduce((pre, curr) => {
      return pre + curr;
    }, 0);
    if (sum > target) return;
    if (sum == target) {
      res.push([...path]);
      return;
    }
    for (let i = index; i < n.length; i++) {
      if (i - 1 >= index && candidates[i - 1] == candidates[i]) {
        continue;
      }
      path.push(n[i]);
      backTracking(n, i + 1);
      path.pop();
    }
  }
  backTracking(candidates, 0);
  return res;
};
