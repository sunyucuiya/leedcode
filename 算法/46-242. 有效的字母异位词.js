/**
 * 哈希表
 * 1.数组
 * 2. set
 * 3. map 
 * **js 取字符的ASCII码是'a'.charCodeAt()
 * s[i].charCodeAt()-'a'.charCodeAt()得到相对位置 
 * 例如 a-a 对应数组的0
 */
var isAnagram = function(s, t) {
    let hash = new Array(26).fill(0);
    for(let i=0;i<s.length;i++){
        hash[s[i].charCodeAt()-'a'.charCodeAt()]++;
    }
    for(let i=0;i<t.length;i++){
        hash[t[i].charCodeAt()-'a'.charCodeAt()]--;
    }
    for(let i=0;i<hash.length;i++){
        if(hash[i]!==0) return false;
    }
    return true;
};