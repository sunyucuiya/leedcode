/**
给你一个字符串数组，请你将 字母异位词 组合在一起。可以按任意顺序返回结果列表。
字母异位词 是由重新排列源单词的所有字母得到的一个新单词。
示例 1:
输入: strs = ["eat", "tea", "tan", "ate", "nat", "bat"]
输出: [["bat"],["nat","tan"],["ate","eat","tea"]]
 */
/**
 * 比较复杂的数据结构用 对象最为hash表 （方便查询）
 * key:每个单词出现的次数
 */
/**
    map:{自定义的查询key: 最后的res}
    初始化数组： new Array(arrLength).fill(initValue)
    字符askii对应的数值： c.charCodeAt();
 */
var groupAnagrams = function(strs) {
    const map = new Object()
    for(let s of strs){ 
        const hash = new Array(26).fill(0); //构建一个查询hash表，根据字母特定的位置记录次数，hash可以用数组来实现
        for(let i of s){ 
            hash[i.charCodeAt()-'a'.charCodeAt()]++; //利用相对于a的askii码值得到字母的位置，并记录次数
        }
        map[hash] ? map[hash].push(s) : map[hash] = [s]; 
    }
    return Object.values(map)
};