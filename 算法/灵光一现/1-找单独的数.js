/**
 * 在一个班级中，每位同学都拿到了一张卡片，上面有一个整数。
 * 有趣的是，除了一个数字之外，所有的数字都恰好出现了两次。
 * 现在需要你帮助班长小C快速找到那个拿了独特数字卡片的同学手上的数字是什么。
 */

/**
 * 基本思路：用哈希表（对象）记录每个元素出现的次数，找到次数是1的返回
 * 扩展 可以找到出现的最大次数和最小次数
 */
function findUniqueNumber0(cards) {
  const map = {};
  for(let i = 0;i<cards.length;i++) {
    map[cards] ? map[cards]++ : map[cards] = 1;
  }
  let res = null;
  for(let key in map) {
    if(map[key] === 1) {
      res = key;
      break;
    }
  }
  return Number(res);
}
/**
 * 灵光一现🌟
 * 用异或运算 ^
 * 特性 （1）与自身异或为0 （2）与0异或为自身 （3）异或满足交换律
 * a^a^a=a
 * 由于除了一个数字之外，所有的数字都恰好出现了两次，所以异或后为0，再与那个出现一次的数字异或，结果就是那个数字
 */

function findUniqueNumber(cards) {
  let res=0;
  for(let i=0;i<cards.length;i++) {
    res^=cards[i]
  }
  return res;
}
const cards = [1, 1, 2, 2, 3, 3, 4, 5, 5];
console.log('找单独的数', findUniqueNumber0(cards))

/**
 * 思考
 * 数字卡片 有偶数个和奇数个的数字，异或可以找到奇数个数的数
 */