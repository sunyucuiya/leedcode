var swapPairs = function(head) {
    let dummyHead = new ListNode(0,head);
    let curr = dummyHead;
    while(curr.next&&curr.next.next){
        let temp0 = curr.next;
        let temp1 = curr.next.next.next;
        curr.next = curr.next.next;
        curr.next.next = temp0;
        curr.next.next.next = temp1;
        curr = curr.next.next;
    }
    return dummyHead.next;
};