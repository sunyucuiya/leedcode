/**
给定一个未排序的整数数组 nums ，找出数字连续的最长序列（不要求序列元素在原数组中连续）的长度。
请你设计并实现时间复杂度为 O(n) 的算法解决此问题。
示例 1：
输入：nums = [100,4,200,1,3,2]
输出：4
解释：最长数字连续序列是 [1, 2, 3, 4]。它的长度为 4。
 */
/**
 * 1 用set去重
 * 2 找到最大的起点位置 利用set去查找有没有 curr-1
 * 3 找到后面的数字 curr+1存在，更新长度
 * 4 更新最大长度
 */
/**
 * @param {number[]} nums
 * @return {number}
 */
var longestConsecutive = function (nums) {
  const set = new Set(nums); //去重
  let maxLen = 0;
  for (let i = 0; i < nums.length; i++) {
    if(!set.has(nums[i] - 1)) { //最长的起点位置
      let curr = nums[i];
      let count = 1;
      while(set.has(curr + 1)) {
        curr++;
        count++;
      }
      maxLen = Math.max(maxLen, count);
    }
  }
};
