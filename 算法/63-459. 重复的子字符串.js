var repeatedSubstringPattern = function (s) {
  let slen = s.length;
  if (slen == 0) return false;
  let next = getNext(s);
  // console.log(next)
  let nextLen = next.length;
  /**
   * next[nextLen - 1] 最长相等前后缀的长度
   * 重复子串就是 slen-next[nextLen - 1]
   * 1-存在重复串
   * 2-能够整除-代表由重复串组成
   */
  if (next[nextLen - 1] !==0 && slen%(slen-next[nextLen - 1])==0) {
    return true;
  }
  return false;
};
var getNext = (s) => {
  let next = [];
  let j = 0;
  next.push(j);
  for (let i = 1; i < s.length; i++) {
    while (j > 0 && s[i] !== s[j]) {
      j = next[j - 1];
    }
    if (s[i] == s[j]) {
      j++;
    }
    next.push(j);
  }
  return next;
}
// repeatedSubstringPattern('aabaaf');