/**
 * 存在无限循环就是sum重复出现
 * 哈希记录sum出现的次数 
 */
var isHappy = function(n) {
    let map={}
    if(n==1) return true;
    let sum = n;
    while(sum!==1){
        let arr = [...sum.toString()].map(i=>i*i);
        sum = arr.reduce((x,y)=> x+y);
        if(map[sum]){
            return false;
        }else{
            map[sum]=1;
        }
    }
    return true
};
isHappy(19)
