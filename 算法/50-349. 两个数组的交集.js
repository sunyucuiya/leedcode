var intersection = function(nums1, nums2) {
    let arr = nums1.filter(i=>nums2.includes(i));
    return arr.filter((i,index,arr)=> arr.indexOf(i)==index);
  };