/**
 * 反转字符串
 * 1-库函数
 * 2-双指针
 */
var reverseString = function (s) {
    return s.reverse();
};
// 双指针
var reverseString = function (s) {
    let left = 0;
    let right = s.length - 1;
    while(left<right){
      [s[left],s[right]] = [s[right],s[left]];
      left++;
      right--;
    }
    return [...s];
};