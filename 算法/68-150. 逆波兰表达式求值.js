/**
 * 后缀表达式-后序遍历 也是计算机处理计算的方式
 * 单竖杠0是取整 a/b|0 保留整数部分
 * https://blog.csdn.net/u012857153/article/details/61922729
 * isNaN(tokens[i])判断非数字字符
 * 
 */
var evalRPN = function (tokens) {
  let stack = []
  for (let i = 0; i < tokens.length; i++) {
    if (isNaN(tokens[i])){
        let a = stack.pop();
        let b = stack.pop();
        stack.push(oper(b,a,tokens[i]))
      }else{
        stack.push(tokens[Number(i)]);
      }
  }
  return stack[0];
};
var oper = (a, b, op) => {
  if (op == '+') return a+b;
  if (op == '-') return a-b;
  if (op == '*') return a*b;
  if (op == '/') return a/b|0;
}