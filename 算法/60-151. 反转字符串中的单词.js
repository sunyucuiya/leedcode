/**
 * 
 */
var reverseWords = function (s) {
  let arr = s.split(' ');
  arr = arr.filter(each => each.length!==0).reverse();
  return arr.join(' ');
};