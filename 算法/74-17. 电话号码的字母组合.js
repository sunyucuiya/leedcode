
/**
 * 定义成全局变量会出错！！
 */
var letterCombinations = function (digits) {
  let letter = ['', '', 'abc', 'def', 'ghi', 'jkl', 'mno', 'pqrs', 'tuv', 'wxyz'];
  let res = [];
  let path = [];
  const k = digits.length;
  if (!k) return [];
  if (k == 1) return letter[digits[0]].split('')
  backTracking(digits, k, 0);
  var backTracking = (digits, k, index) => {
    if (path.length === k) {
      res.push(path.join(''));
      return;
    }
    for (let item of letter[digits[index]]) {
      path.push(item);
      backTracking(digits, k, index + 1);
      path.pop();
    }
  }
  return res;
};
