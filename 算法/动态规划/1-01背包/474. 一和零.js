/**
01背包装的最大个数
给你一个二进制字符串数组 strs 和两个整数 m 和 n 。
请你找出并返回 strs 的最大子集的长度，该子集中最多有m个0 和 n个1 。
如果 x 的所有元素也是 y 的元素，集合 x 是集合 y 的 子集 。

01怎么计算个数？？
输入：strs = ["10", "0001", "111001", "1", "0"], m = 5, n = 3
输出： 4

dp[i][j] 背包容量m;n装的最大个数  dp[i][m][n]==>dp[m][n]
容量是两维
zero: m; one: n
dp[i][j]=max(dp[i][j],dp[i-zeros][j-ones]+1)
初始化？每个元素的01个数
遍历 容量倒序 i=m->zeros j=n->ones
 */
/**
 * 思考：边界条件当作背包容量
 */
var findMaxForm = function(strs, m, n) {
  const dp = Array(m+1).fill().map(i=>Array(n+1).fill(0));
  for(let s of strs){
    let ones = 0;
    let zeros = 0;
    for(let a of s){
      a==='0' ? zeros++ : ones++;
    }
    for(let i=m;i>=zeros;i--){
      for(let j=n;j>=ones;j--){
        dp[i][j] = Math.max(dp[i][j], dp[i-zeros][j-ones]+1);
      }
    }
  }
  return dp[m][n];
};
/**
review1211✅
 */
const strs = ['10', '0001', '111001', '1', '0'];
const m = 5;
const n = 3;
// console.log('一和零', fn(strs, m, n));