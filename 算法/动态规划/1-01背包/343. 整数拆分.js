/**
 * 给定一个正整数 n ，将其拆分为 k 个 正整数 的和（ k >= 2 ），
 * 并使这些整数的乘积最大化
 * 返回 你可以获得的最大乘积
 */

/** 01背包
dp[i] i~n整数拆分后得到的最大乘积数
【提取出一个元素变量--和数】j是i的一个和数
【分析所有可能性，把公式都列上】
1-两个和数： dp[i] = j*(i-j)    i=j+(i-j)
2-多个和数： dp[i] = dp[i-j] * j     i=j1+j2+j3+...+jx
【保留最大值】
dp[i] =max(dp[i], max(j*(i-j), dp[i-j] * j ))

初始化： dp[2]=1
遍历： 两层循环
下标推进： i==>n

和数：j=1==>i/2 减少重复计算

 */
 
var integerBreak = function(n) {
  const dp = Array(n+1).fill(0);
  dp[2] = 1; //初始化
  for(let i=3; i<=n; i++) {
    for(let j=1; j<=i/2; j++) { //减少重复计算
      dp[i] = Math.max(dp[i], Math.max(j*(i-j), dp[i-j]*j))
    }
  }
  return dp[n];
};
/**
 * review1210✅
 */
//n = 10 //36
console.log('整数拆分', integerBreak(10));