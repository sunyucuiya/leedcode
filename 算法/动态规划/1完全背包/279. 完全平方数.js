/**
 给你一个整数 n ，返回 和为 n 的完全平方数的最少数量 。
完全平方数 是一个整数，其值等于另一个整数的平方；
换句话说，其值等于一个整数自乘的积。
例如，1、4、9 和 16 都是完全平方数，而 3 和 11 不是。
输入：n = 12
输出：3 
解释：12 = 4 + 4 + 4
 */
/**
每个完全平方数可以无限使用 ---完全背包
容量n 物品：提取n的所有完全平方数 i*i
dp[i] 容量为i的背包所装元素最小个数
dp[i] = min(dp[i], dp[i-x])
初始化：每个元素设置最大值 dp[0]=0;
遍历： 先背包/
 */
var numSquares = function(n) {
  const dp=Array(n+1).fill(Infinity);
  dp[0]=0;
  for(let i=1;i<=n;i++){
    //! 物品数取决于背包容量 所以先遍历容量
    for(let j=1;j*j<=i;j++){
      dp[i] = Math.min(dp[i], dp[i-j*j]+1);
    }
  }
  return dp[n];
};

console.log('完全平方数',numSquares(12));

/**
 * review 1214✅
 */