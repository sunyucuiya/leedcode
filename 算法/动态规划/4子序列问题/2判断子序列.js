/**
 * 判断子序列
给定字符串 s 和 t ，判断 s 是否为 t 的子序列。

【不需要连续】字符串的一个子序列是原始字符串删除一些（也可以不删除）字符而不改变剩余字符相对位置形成的新字符串。
（例如，"ace"是"abcde"的一个子序列，而"aec"不是）。
输入：s = "abc", t = "ahbgdc"
输出：true
 */

/**
 * 思路，最长公共子序列是s?
 * 🤔️最长公共子序列 的到的是长度， 但是子序列不知道，怎么记录？
 * 😂 直接比较长度啊！！
 */
var isSubsequence = function(s, t) {
  const [m,n] = [t.length, s.length];
  const dp = Array(m+1).fill().map(() => Array(n+1).fill(0))
  let res = 0;
  for(let i=1;i<=m;i++) {
    for(let j=1;j<=n;j++) {
      if(t[i-1] == s[j-1]) {
        dp[i][j] = dp[i-1][j-1]+1;
      } else {
        dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1])
      }
      res = Math.max(res, dp[i][j]);
    }
  }
  return res == n;
};
const s = "abc", t = "ahbgdc";
console.log('判断子序列', isSubsequence(s, t) ); //true

