/**
给你一个整数数组 nums ，请你找出一个具有最大和的**连续**子数组（子数组最少包含一个元素），返回其最大和。
子数组是数组中的一个连续部分。
输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
输出：6
解释：连续子数组 [4,-1,2,1] 的和最大，为 6 。
 */
/**
 * 【连续】i i-1
 * dp[i]是nums[i]结尾的最大子序和
 * dp[i]= Max（之前计算dp[i-1]+nums[i]、重新计算nums[i]）
 */
var maxSubArray = function(nums) {
  const m = nums.length;
  const dp = Array(m+1).fill(0);
  dp[0] = nums[0];
  let res = nums[0];
  for(let i=1;i<m;i++) {
    dp[i] = Math.max(dp[i-1]+nums[i], nums[i]);
    res = Math.max(res, dp[i])
  }
  return res;
};
const nums = [-2,1,-3,4,-1,2,1,-5,4];
console.log('最大子序和', maxSubArray(nums))//6