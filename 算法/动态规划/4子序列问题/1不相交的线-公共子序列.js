/**
 * 
在两条独立的水平线上按给定的顺序写下 nums1 和 nums2 中的整数。
现在，可以绘制一些连接两个数字 nums1[i] 和 nums2[j] 的直线，这些直线需要同时满足：
nums1[i] == nums2[j]
且绘制的直线不与任何其他连线（非水平线）相交。
请注意，连线即使在端点也不能相交：每个数字只能属于一条连线。（元素相同，相对位置也相同 --》最长公共子序列）
以这种方法绘制线条，并返回可以绘制的最大连线数。

输入：nums1 = [1,4,2], nums2 = [1,2,4]
输出：2
输入：nums1 = [2,5,1,2,5], nums2 = [10,5,2,1,5,2]
输出：3
 */
var maxUncrossedLines = function(nums1, nums2) {
  const [m,n] = [nums1.length, nums2.length];
  const dp = Array(m+1).fill().map(() => Array(n+1).fill(0));
  for(let i=1; i<=m; i++) {
    for(let j=1; j<=n;j++) {
      if(nums1[i-1] === nums2[j-1]) {
        dp[i][j] = dp[i-1][j-1]+1;
      } else {
        dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
      }
    }
  }
  return dp[m][n];
};
const nums1 = [1,4,2];
const nums2 = [1,2,4];
console.log('不相交的线', maxUncrossedLines(nums1, nums2)) //2