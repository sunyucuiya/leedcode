/**
 * 判断一个字符串是否是回文字符串
 * dp[i][j] s[i...j]
 * s[i]==[j] 
 * dp[i][j]=true （j-i<2， dp[i+1][j-1] == true）    
 * s[i]!==[j]
 * dp[i][j] = false 
 */
var isPalindrome = (s)=> {
  const n = s.length;
  const dp = Array(n).fill().map(() => Array(n).fill(false))
  for (let i = 0; i < n; i++) {
    dp[i][i] = true;
  }
  for(let i=n-1; i>=0;i--) {
    for(let j=i+1; j<n;j++) {
      if(s[i]==s[j]&& (dp[i+1][j-1]||j-i<2)){
        dp[i][j]=true;
      }
    }
  }
  return dp[0][n-1];
}
const s = 'abca'

/**
 * 双指针的方法
 */
var isPalindrome0 = s => {
  let res = true;
  for (let i = 0, j = s.length - 1; i < j; i++, j--) {
    if (s[i] !== s[j]) {
      res = false;
      break;
    }
  }
  return res;
};

console.log('判断一个字符串是否是回文字符串', isPalindrome0(s))
