/**
 * 递增子序列--可以不连续
给你一个整数数组 nums ，找到其中最长严格递增子序列的长度。
输入：nums = [10,9,2,5,3,7,101,18]
输出：4
解释：最长递增子序列是 [2,3,7,101]，因此长度为 4 。
 */
/**
 * 【不连续】i,j=0~i 之间的关系 
 *  dp更新 一维数组 自己更新自己
 * dp[i] 是以nums[i]结尾的子序列的长度， 最优结果产生在过程中，需要res记录
 */
var lengthOfLIS = function(nums) {
  const dp = Array(nums.length).fill(1);
  let res = 1;
  for(let i=1;i<nums.length;i++) {
    for(let j=0; j<i ;j++) {
      if(nums[i]>nums[j]) {
        dp[i] = Math.max(dp[i], dp[j]+1)
      }
    }
    res = Math.max(res, dp[i]);
  }
  return res;
};
const nums = [10,9,2,5,3,7,101,18];
console.log('最长递增子序列',lengthOfLIS(nums));//4