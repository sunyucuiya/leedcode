/**
 * 递增子序列--连续
 * 给你一个整数数组 nums, 找到最长且 **连续递增**的子序列，并返回该序列的长度
输入：nums = [1,3,5,4,7]
输出：3
解释：最长连续递增序列是 [1,3,5], 长度为3。5 和 7 在原数组里被 4 隔开。
 */
var findLengthOfLCIS = function(nums) {
  const len = nums.length;
  const dp = Array(len).fill(1);
  for(let i=1;i<len;i++) {
    if(nums[i]>nums[i-1]) {
      dp[i] = dp[i-1]+1;
    }
  }
  return Math.max(...dp);
};
const nums = [1,3,5,4,7];
console.log('递增子序列--连续',findLengthOfLCIS(nums));//3