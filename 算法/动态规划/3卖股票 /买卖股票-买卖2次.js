/**
 * 买卖股票
给定一个数组，它的第 i 个元素是一支给定的股票在第 i 天的价格。
设计一个算法来计算你所能获取的最大利润。你最多可以完成 **两笔** 交易。
注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。
输入：prices = [3,3,5,0,0,3,1,4]
输出：6
 */
/**
 * 5种状态
 * dp[0] 没有买卖 没有操作
 * dp[1] 第一次持有
 * dp[2] 第一次不持有
 * dp[3] 第二次持有
 * dp[4] 第二次不持有
 */
var maxProfit = function(prices) {
  const dp = Array(5).fill(0);

  dp[1]=-prices[0];
  dp[3]=-prices[0];

  for(let i=1;i<prices.length;i++) {
    dp[1] = Math.max(dp[1], dp[0]-prices[i]);
    dp[2] = Math.max(dp[2], dp[1]+prices[i]);
    dp[3] = Math.max(dp[3], dp[2]-prices[i]);
    dp[4] = Math.max(dp[4], dp[3]+prices[i]);
  }
  return dp[4];
};
const prices = [3,3,5,0,0,3,1,4];
console.log('买卖股票-买两次',maxProfit(prices)); //6
