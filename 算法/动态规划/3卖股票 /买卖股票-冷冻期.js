/**
给定一个整数数组prices，其中第  prices[i] 表示第 i 天的股票价格 。​
设计一个算法计算出最大利润。在满足以下约束条件下，你可以尽可能地完成更多的交易（多次买卖一支股票）:
卖出股票后，你无法在第二天买入股票 (即冷冻期为 1 天)。
注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。
输入: prices = [1,2,3,0,2]
输出: 3 
解释: 对应的交易状态为: [买入, 卖出, 冷冻期, 买入, 卖出]
 */

/**
 * 多了冷冻期状态
 * dp[i][0] 持有(买过， 刚果冷冻期买， 过了冷冻期很久了买) max(dp[i][0], dp[i-1][3]-prices[i], dp[i-1][1]-prices[i])
 * dp[i][1] 不持有(卖过， 刚过冷冻期) max(dp[i-1][1], dp[i-1][3])
 * dp[i][2] 当天卖出(不持有) =dp[i-1][0]+pricecs[i]
 * dp[i][3] 冷冻期（不持有） =dp[i][2]
 */
var maxProfit = function(prices) {
  const m = prices.length
  let dp = Array(m).fill().map(() => Array(4).fill(0)); //!注意初始化方式
  dp[0][0] = -prices[0];
  for(let i=1;i<m;i++){
    dp[i][2] = dp[i-1][0]+prices[i];
    dp[i][3] = dp[i-1][2];
    dp[i][0] = Math.max(dp[i-1][0], dp[i-1][1]-prices[i], dp[i-1][3]-prices[i]);
    dp[i][1] = Math.max(dp[i-1][1], dp[i-1][3]);
    
  }
  return Math.max(...dp[m-1]);
};

var maxProfit0 = function(prices) {
  const m = prices.length
  let dp = Array(4).fill(0); //!注意初始化方式
  dp[0] = -prices[0];
  for(let i=1;i<m;i++){
    //缓存
    const t2 = dp[0]+prices[i];
    const t3 = dp[2];
    dp[0] = Math.max(dp[0], dp[1]-prices[i], dp[3]-prices[i]);
    dp[1] = Math.max(dp[1], dp[3]);
    dp[2] = t2;
    dp[3] = t3;
    
  }
  return Math.max(...dp);
};
const prices = [1,2,3,0,2];
console.log('买卖股票-冷冻期',maxProfit0(prices));//3