/**
 打家劫舍-1
 你是一个专业的小偷，计划偷窃沿街的房屋。
 每间房内都藏有一定的现金，影响你偷窃的唯一制约因素就是相邻的房屋装有相互连通的防盗系统，
 如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警。
 给定一个代表每个房屋存放金额的非负整数数组，计算你 不触动警报装置的情况下 ，一夜之内能够偷窃到的最高金额。
输入：[1,2,3,1]
输出：4
 */
/**
 * 不能连续偷两家
 * dp[i] 1~m家 能偷到的最大金额
 * dp[i] = max(dp[i-1], dp[i-2]+nums[i])
 */
const rob = (nums) => {
  const m = nums.length;
  const dp = Array(m).fill(0);
  dp[0] = nums[0];
  dp[1] = Math.max(nums[0], nums[1]);
  //一定是从前往后
  for(let i=2;i<m;i++) {
    dp[i] =Math.max(dp[i-1], dp[i-2]+nums[i]);
  }
  return dp[m-1];
}
const nums = [1,2,3,1];
// console.log('打家劫舍-1',rob(nums));






