/**
 * 打家劫舍-2
 * 这个地方所有的房屋都 围成一圈 ，这意味着第一个房屋和最后一个房屋是紧挨着的
 * 输入： nums = [2,3,2]
 * 输出：3
 */

/**
 * 偷第一家，就不能偷最后一家
 * 偷最后一家，就不能偷第一家
 * 两种情况，取最大值
 * res = Math.max(rob(nums.slice(0,nums.length-1), rob(nums.slice(1,nums.length)))
 */

const rob2 = (nums) => {
  const rob = (nums) => {
    const m = nums.length;
    const dp = Array(m).fill(0);
    dp[0] = nums[0];
    dp[1] = Math.max(nums[0], nums[1]);
    //一定是从前往后
    for(let i=2;i<m;i++) {
      dp[i] =Math.max(dp[i-1], dp[i-2]+nums[i]);
    }
    return dp[m-1];
  }
  return Math.max(rob(nums.slice(0,nums.length-1)), rob(nums.slice(1,nums.length)));
}

console.log('打家劫舍-2',rob2([2,3,2]));//3