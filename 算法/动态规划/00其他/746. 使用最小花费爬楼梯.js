/** 
给你一个整数数组 cost ，其中 cost[i] 是从楼梯第 i 个台阶向上爬需要支付的费用。
一旦你支付此费用，即可选择向上爬一个或者两个台阶。
你可以选择从下标为 0 或下标为 1 的台阶开始爬楼梯。
请你计算并返回达到楼梯顶部的最低花费。
dp[i] 到达第i个台阶的最低花费
花费1 dp[i-1]+cost[i-1]
花费2 dp[i-2]+cost[i-2]
公式 dp[i] = Math.min(dp[i-1]+cost[i-1], dp[i-2]+cost[i-2])
初始化
dp[0]=0
dp[1]=0
遍历：i= 2==>cost.length
 * @param {*} cost 
 * @returns 
 */
var minCostClimbingStairs = function(cost) {
  const dp = [0, 0];
  for(let i=2;i<=cost.length;i++){
    dp[i] = Math.min(dp[i-1]+cost[i-1], dp[i-2]+cost[i-2])
  }
  return dp[cost.length];
};