/**
给定一个三角形 triangle ，找出自顶向下的最小路径和。
每一步只能移动到下一行中相邻的结点上。
相邻的结点 在这里指的是 下标 与 上一层结点下标 相同或者等于 上一层结点下标 + 1 的两个结点。
也就是说，如果正位于当前行的下标 i ，那么下一步可以移动到下一行的下标 i 或 i + 1 。
输入：triangle = [[2],[3,4],[6,5,7],[4,1,8,3]]
输出：11
解释：如下面简图所示：
   2
  3 4
 6 5 7
4 1 8 3
自顶向下的最小路径和为 11（即，2 + 3 + 5 + 1 = 11）。
 */
/**
 * dp一维数组，不断更新维护
 * 【自上而下】
 * 第一个元素和最后一个元素的来源只有一个
 * 最右侧元素： dp[j] = triangle[i][j]+ dp[j-1]
 * 最左侧元素: dp[j] = triangle[i][j] + dp[j]
 * 中间元素有两个来源： dp[j] = triangle[i][j]+min(dp[j], dp[j-1])
 * 
 */
var minimumTotal = function(triangle) {
  const n = triangle.length;
  const dp = Array(n).fill(0);
  dp[0] = triangle[0][0];
  for(let i=1;i<n;i++) {
    for(let j=i;j>0; j--) {
      if(j==i){
        dp[j] = triangle[i][j] +dp[j-1]; //从右向左更新 防止dp[j]的更新被覆盖掉
      } else {
        dp[j] = triangle[i][j] +Math.min(dp[j], dp[j-1]);
      }
    }
    dp[0] = triangle[i][0] + dp[0];
  }
  return Math.min(...dp);

};
const triangle = [[2],[3,4],[6,5,7],[4,1,8,3]];
console.log('三角形最小路径和',minimumTotal(triangle)); //11