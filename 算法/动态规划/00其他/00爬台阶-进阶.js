/**
假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
每次你可以爬至多m (1 <= m < n)个台阶。你有多少种不同的方法可以爬到楼顶呢？
 */
/**
 * 完全背包问题
 * 容量：n台阶数；物品：每次爬1～m阶 
 * 有几种爬台阶方式？ 1，2；2，1是两种方案 所以是排列数 外层容量 内层物品
 */
const climbingStairs = (n, m) => {
  const dp = Array(n+1).fill(0);
  dp[0]=1;
  for(let i=1;i<=n;i++){ //从1阶开始
    for(let j=1;j<=m;j++){ //每次可以爬1～m阶
      if(i>=j){
        dp[i] += dp[i-j];
      }
    }
  }
  return dp[n];
}
console.log('爬楼梯-进阶', climbingStairs(3,2))