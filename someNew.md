[taplo](https://taplo.tamasfe.dev/)    
不知道这个是干嘛用的但是看起来很厉害  
> Taplo 是一个针对 TOML 格式文件的语法高亮、自动补全和 linting 插件，适用于多种代码编辑器和 IDE。TOML（Tom's Obvious, Minimal Language）是一种简单的配置文件格式，常用于各种项目作为其配置的标准格式。   
>   
使用场景  
- 项目配置
- 应用配置
- 静态网站生成器
- 开源项目  

[mitt](https://github.com/developit/mitt)
一个体积很小的事件分发器